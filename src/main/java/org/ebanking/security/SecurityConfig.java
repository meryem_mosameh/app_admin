package org.ebanking.security;

import javax.sql.DataSource;

import org.ebanking.controller.LoggingAccessDeniedHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired
	private DataSource dataSource;
	@Autowired
    private LoggingAccessDeniedHandler accessDeniedHandler;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		//auth.inMemoryAuthentication().withUser("admin").password("123456");
		auth.jdbcAuthentication().dataSource(dataSource).usersByUsernameQuery("select username as principle,password as credentials from admin where username=?");
		
	}
	
	
	
	/*protected void configure(HttpSecurity http) throws Exception {
		http.formLogin();
		http.authorizeRequests().antMatchers("/saveAgent/**","/deleteAgent/**").hasRole("ADMIN");
		//http.authorizeRequests().antMatchers("/saveClient/**","/deleteClient/**").hasRole("AGENT");
		http.authorizeRequests().anyRequest().authenticated();
		http.csrf();//prevention du csrf
		http.authorizeRequests().anyRequest().permitAll();
		
	}*/
	@Bean
	BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
/*	@Override
	protected void configure(HttpSecurity http) throws Exception{
		http.formLogin().loginPage("/login");
		http.authorizeRequests().antMatchers("/createAgence/**","/ajouterAgent/**","supprimerAgent/**","changeAgence/**").hasRole("ADMIN");
		http.authorizeRequests().anyRequest().authenticated();
		http.csrf();
		http.authorizeRequests().anyRequest().permitAll();
	}*/
	
	
	 @Override
	    protected void configure(HttpSecurity http) throws Exception {
	        http
	                .authorizeRequests()
	                    .antMatchers(
	                            "/",
	                            "/js/**",
	                            "/css/**",
	                            "/img/**",
	                            "/webjars/**").permitAll()
	                    .antMatchers("/user/**").hasRole("USER")
	                    .anyRequest().authenticated()
	                .and()
	                .formLogin()
	                    .loginPage("/login")
	                    .permitAll()
	                .and()
	                .logout()
	                    .invalidateHttpSession(true)
	                    .clearAuthentication(true)
	                    .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
	                    .logoutSuccessUrl("/login?logout")
	                    .permitAll()
	                .and()
	                .exceptionHandling()
	                    .accessDeniedHandler((AccessDeniedHandler) accessDeniedHandler);
	    }
	 
	 @Bean
	 public AccessDeniedHandler accessDeniedHandler(){
	     return new LoggingAccessDeniedHandler();
	 }

	
}