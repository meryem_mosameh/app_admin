package org.ebanking.repository;

import org.ebanking.entities.Agent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "http://localhost:4200" , allowedHeaders = "*")
@RepositoryRestResource
public interface AgentRepository extends JpaRepository<Agent, Long> {

}
