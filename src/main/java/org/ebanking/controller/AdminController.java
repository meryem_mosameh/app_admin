package org.ebanking.controller;

import org.ebanking.entities.Admin;
import org.ebanking.entities.Agence;
import org.ebanking.entities.Agent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController

@Component

/*	@Bean	*/

public class AdminController {
	
//	@Autowired
	private Admin administrateur;
	
//	@RequestMapping(value="/login")
//	public String ConnectSubmit(Model model) {
//		model.addAttribute("login", new Admin());
//		return "/accueil";
//	}
	
	@RequestMapping(value="/nvlAgence")
	public String nvlAgenceSubmit(Model model) {
		model.addAttribute("newAgence", new Agence());
		return "/result1";
	}
	
	@RequestMapping(value="/nvlAgent")
	public String nvlAgentSubmit(Model model) {
		model.addAttribute("newAgent", new Agent());
		return "/result2";
	}
	
	@RequestMapping(value="/suppAgent")
	public String suppAgentSubmit(Model model) {
		model.addAttribute("suppAgent", new Agent());
		return "/result3";
	}
	
	@RequestMapping(value="/modifAgence")
	public String modifAgenceSubmit(Model model) {
		model.addAttribute("newAgence", new Agence());
		return "/result4";
	}
	
}
