package org.ebanking.service;

import javax.transaction.Transactional;

import org.ebanking.entities.Agence;
import org.ebanking.entities.Agent;
import org.ebanking.repository.AgenceRepository;
import org.ebanking.repository.AgentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
@Transactional

@Component
public class AdminServiceImpl implements IAdminService{
	
	@Autowired
	private AgentRepository agentRepository ;
	@Autowired
	private AgenceRepository agenceRepository ;
	

	@Override
	public Agence createAgence(Agence agence) {
		return agenceRepository.save(agence);
	}

	@Override
	public Agent ajouterAgent(Agent agent) {
		return agentRepository.save(agent);
		
	}

	@Override
	public void supprimerAgent(Agent agent) {
		agentRepository.delete(agent);
		
	}

	@Override
	public Agent changeAgence(Agence agence,Agent agent) {
		agent.setAgence(agence);
		return agentRepository.save(agent);
	}

	
}
