package org.ebanking.service;

import org.ebanking.entities.Agence;
import org.ebanking.entities.Agent;

public interface IAdminService {
	
	public Agence createAgence(Agence agence);
	public Agent ajouterAgent(Agent agent);
	public void supprimerAgent(Agent agent);
	public Agent changeAgence(Agence agence,Agent agent);
	
	

}
