package org.ebanking.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotEmpty;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "users")

@Component
public class User {

    @Id
    @NotEmpty(message = "*Please provide a username")
    @Column(length = 50)
    private String username;
    
  
    @NotEmpty(message = "*Please provide your password")
    @Column(length = 100)
    private String password;
    
    
    private Boolean active;
    
    
    @ManyToMany
    @JoinTable(
    		joinColumns = @JoinColumn(name = "username"), 
    		inverseJoinColumns = @JoinColumn(name = "role"))
    private List<Role> roles;
    
    public void addRole(Role role1) {
		if (roles == null) {
			roles = new ArrayList<>();
		}
		roles.add(role1);

	}
}
