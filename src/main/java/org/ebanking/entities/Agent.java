package org.ebanking.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@ToString
@Data
@NoArgsConstructor

@Component
public class Agent {

	public Agent(String nom, String prenom, Agence agence) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.agence = agence;
	}
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long code;
	private String nom;
	private String prenom;
	@ManyToOne
	@JoinColumn(name = "CODE_AGENCE")
	private Agence agence;
	

}