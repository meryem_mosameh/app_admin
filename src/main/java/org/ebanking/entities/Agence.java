package org.ebanking.entities;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@ToString
@Data
@NoArgsConstructor

@Component
public class Agence {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nom;
	private String adresse;
	@OneToMany(mappedBy = "agence",fetch=FetchType.LAZY)
	private Collection<Agent> agents;
	
	
	public Agence(String nom, String adresse) {
		super();
		this.nom = nom;
		this.adresse = adresse;
		this.agents = agents;
	}

	
	
	

}